#import <CoreMotion/CoreMotion.h>
#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>

#define IMAGE_NUMBER @"ImageNumber"

#define CALLBACK_FUNCTION @"pictureTakenCallback"

extern UIViewController *UnityGetGLViewController();

@interface UnityCameraPlugin : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate>
{
    UIImagePickerController *imagePickerController;
    CLLocationManager *locationManager;
    CMMotionManager *motionManager;
    UIAlertView *alertView;
}

@property (strong, nonatomic) NSString *gameObjectName;
@property (strong, nonatomic) NSString *output;
@property (strong, nonatomic) NSString *imagePath;
@property (strong, nonatomic) NSString *latitude;
@property (strong, nonatomic) NSString *longitude;
@property (strong, nonatomic) NSString *azimuth;
@property (strong, nonatomic) NSString *pitch;
@property (strong, nonatomic) NSString *roll;

@end

@implementation UnityCameraPlugin;

#pragma mark - Initialize Objects of This Plugin Class.

- (void)initializeObjects
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && (imagePickerController == nil)) {
        imagePickerController = [UIImagePickerController new];
        imagePickerController.delegate = self;
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePickerController.showsCameraControls = YES;
    }
    
    if (alertView == nil) {
        alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
    
    if (locationManager == nil) {
        locationManager = [CLLocationManager new];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    }
    
    if (motionManager == nil) {
        motionManager = [CMMotionManager new];
        motionManager.deviceMotionUpdateInterval = 1.0/60.0;
    }
    
    if ([[NSUserDefaults standardUserDefaults] integerForKey:IMAGE_NUMBER] < 0) {
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:IMAGE_NUMBER];
    }
    
    if (self.gameObjectName == nil) {
        self.gameObjectName = [NSString new];
    }
    
    if (self.output == nil) {
        self.output = [NSString new];
    }
    
    if (self.imagePath == nil) {
        self.imagePath = [NSString new];
    }
    
    self.gameObjectName = @"";
    self.output         = @"";
    self.imagePath      = @"";
    self.latitude       = @"";
    self.longitude      = @"";
    self.azimuth        = @"";
    self.pitch          = @"";
    self.roll           = @"";
}





#pragma mark - Handling Device Camera

- (void)launchCameraWithGameObjectName:(NSString *)gameObject
{
    if (gameObject.length > 1) {
        [self initializeObjects];
        
        if ([motionManager isDeviceMotionAvailable]) {
            [motionManager startDeviceMotionUpdates];
        }
        
        if (imagePickerController != nil) {
            self.gameObjectName = [NSString stringWithString:gameObject];
            
            [UnityGetGLViewController() presentViewController:imagePickerController animated:YES completion:^{
                UIViewController *pickerController = (UIViewController *)[imagePickerController.viewControllers objectAtIndex:0];
                
                if ([UIDevice currentDevice].systemVersion.floatValue < 7.0) {
                    UIButton *captureButton = (UIButton *)[[[[[[[pickerController.view.subviews objectAtIndex:4] subviews] objectAtIndex:1] subviews] objectAtIndex:1] subviews] objectAtIndex:0];
                    [captureButton addTarget:self action:@selector(startFetchingLocation) forControlEvents:UIControlEventTouchDown];
                }
                else {
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                        UIButton *captureButton = (UIButton *)[[[pickerController.view.subviews objectAtIndex:1] subviews] objectAtIndex:0];
                        [captureButton addTarget:self action:@selector(startFetchingLocation) forControlEvents:UIControlEventTouchDown];
                    }
                    else {
                        UIButton *captureButton = (UIButton *)[[[pickerController.view.subviews objectAtIndex:2] subviews] objectAtIndex:0];
                        [captureButton addTarget:self action:@selector(startFetchingLocation) forControlEvents:UIControlEventTouchDown];
                    }
                }
            }];
        }
        else {
            [alertView setMessage:@"Unable to detect camera on device."];
            [alertView show];
            
            [motionManager stopDeviceMotionUpdates];
        }
    }
    else {
        [alertView setMessage:@"Please provide a proper game object name."];
        [alertView show];
    }
}

- (void)dismissCameraWithSavedImage
{
    if ( (self.imagePath.length > 0) && (self.latitude.length > 0) && (self.longitude.length > 0) && (self.azimuth.length > 0) && (self.pitch.length > 0) && (self.roll.length > 0) ) {
        
        [locationManager stopUpdatingLocation];
        [locationManager stopUpdatingHeading];
        
        [motionManager stopDeviceMotionUpdates];
        
        self.output = [NSString stringWithFormat:@"%@;%@;%@;%@;%@;%@;", self.imagePath, self.latitude, self.longitude, self.self.azimuth, self.pitch, self.roll];
        
        UnitySendMessage(self.gameObjectName.UTF8String, CALLBACK_FUNCTION.UTF8String, self.output.UTF8String);
        
        if (self.imagePath.length > 1) {
            [alertView setMessage:[NSString stringWithFormat:@"ImagePath : %@\n\n Latitude : %@\n\n Longitude : %@\n\n Azimuth : %@\n\n Pitch : %@\n\n Roll : %@\n\n", self.imagePath, self.latitude, self.longitude, self.azimuth, self.pitch, self.roll]];
            [alertView show];
        }
        
        [UnityGetGLViewController() dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)dismissCameraWithoutSavingImage
{
    [locationManager stopUpdatingLocation];
    [locationManager stopUpdatingHeading];
    
    [motionManager stopDeviceMotionUpdates];
    
    self.output = @";;;;;;";
    
    UnitySendMessage(self.gameObjectName.UTF8String, CALLBACK_FUNCTION.UTF8String, self.output.UTF8String);
    [UnityGetGLViewController() dismissViewControllerAnimated:YES completion:nil];
}





#pragma mark - ImagePickerController Delegates

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSInteger imageNumber = [[NSUserDefaults standardUserDefaults] integerForKey:IMAGE_NUMBER] + 1;
    
    NSString *directoryPath  = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *savedImagePath = [directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"IMAGE_%ld.JPEG", (long)imageNumber]];
    NSData *imageData        = UIImageJPEGRepresentation([info objectForKey:UIImagePickerControllerOriginalImage], 1.0);
    NSError *error           = nil;
    [imageData writeToFile:savedImagePath options:NSDataWritingAtomic error:&error];
    
    if (!error) {
        [[NSUserDefaults standardUserDefaults] setInteger:imageNumber forKey:IMAGE_NUMBER];
        self.imagePath = [NSString stringWithFormat:@"%@", savedImagePath];
        [self dismissCameraWithSavedImage];
    }
    else {
        [alertView setMessage:@"Unable to save image. Please try again"];
        [alertView show];
        [self dismissCameraWithoutSavingImage];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissCameraWithoutSavingImage];
}





#pragma mark - Start Fetching Location

- (void)startFetchingLocation
{
    if ([motionManager isAccelerometerAvailable]) {
        CGFloat pitchTemp = ([motionManager deviceMotion].attitude.pitch < 0)? ((2 * M_PI) + [motionManager deviceMotion].attitude.pitch) : [motionManager deviceMotion].attitude.pitch;
        CGFloat rollTemp = ([motionManager deviceMotion].attitude.roll < 0)? ((2 * M_PI) + [motionManager deviceMotion].attitude.roll) : [motionManager deviceMotion].attitude.roll;
        
        self.pitch   = [NSString stringWithFormat:@"%f", pitchTemp];
        self.roll    = [NSString stringWithFormat:@"%f", rollTemp];
        
        [motionManager stopAccelerometerUpdates];
    }
    else {
        self.pitch = @"Unable to calculate \"Pitch\", as Accelerometer is not found on device.";
        self.roll  = @"Unable to calculate \"Roll\", as Accelerometer is not found on device.";
    }
    
    
    if ([CLLocationManager headingAvailable]) [locationManager startUpdatingHeading];
    else self.azimuth = @"Unable to calculate \"Azimuth\", as Compass is not found on device.";
    
    
    [locationManager startUpdatingLocation];
}

#pragma mark - LocationManager Delegates

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    [manager stopUpdatingLocation];

    self.latitude  = [NSString stringWithFormat:@"%f", manager.location.coordinate.latitude];
    self.longitude = [NSString stringWithFormat:@"%f", manager.location.coordinate.longitude];
    
    [self dismissCameraWithSavedImage];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading
{
    [manager stopUpdatingHeading];

    CGFloat azimuthF = (newHeading.trueHeading > 0.0) ? newHeading.trueHeading : newHeading.magneticHeading;
    self.azimuth = [NSString stringWithFormat:@"%f", azimuthF * M_PI / 180.0];
    
    [self dismissCameraWithSavedImage];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    self.latitude  = @"Unable to fetch \"Latitude\". Please check you GPS setting and Internet Connectivity.";
    self.longitude = @"Unable to fetch \"Longitude\". Please check you GPS setting and Internet Connectivity.";
    
    [self dismissCameraWithSavedImage];
}

@end





#pragma mark - Control Plugin Call

static UnityCameraPlugin *unityCameraPlugin = nil;

extern "C"
{
    void _launchCamera(const char* gameObject)
    {
        if (unityCameraPlugin == nil) unityCameraPlugin = [UnityCameraPlugin new];
        [unityCameraPlugin launchCameraWithGameObjectName:[NSString stringWithCString:gameObject encoding:NSUTF8StringEncoding]];
    }
}
